<?php
/**
 * This is a really bad idea, but it's just an example
 */
namespace Brocoder\FileSystem\Examples;

require_once __DIR__ . '/../vendor/autoload.php';

use Brocoder\FileSystem\SFile;

$counterFile = __DIR__ . '/counter_static.tmp';
$counter = ( int ) SFile::readAll( $counterFile );
SFile::rewrite( $counterFile, ++$counter );