<?php
namespace Brocoder\FileSystem;

class File implements FileI
{
    private const BUFFER_SIZE = 1024;
    private const LINE_MAX_SIZE = 65535;
    
    private $fileName;
    private $isLocked;
    private $fp;
    private $EOL;

    /**
     * @param string $fileName
     * @param bool $lock
     * @throws FileOpeningFailedException
     */
    public function __construct( $fileName, $lock = false )
    {
        $this->fileName = $fileName;
        $this->isLocked = $lock;
        $this->fp = fopen( $fileName, 'a+' );
        if( $this->fp === false ) {
            throw new FileOpeningFailedException( "File '{$this->fileName}' fopen() failed" );
        }
        if( $lock ) {
            flock( $this->fp, LOCK_EX );
        }
        $this->EOL = $this->determineEOL();
    }

    /**
     * @return string
     */
    private function determineEOL()
    {
        $seek = 0;
        fseek( $this->fp, 0 );
        while( ! feof( $this->fp ) ) {
            preg_match( "/((\r\n)|(\n)|(\r))/", $this->read( $seek, self::BUFFER_SIZE ), $matches );
            if( ! empty( $matches[ 0 ] ) ) {
                return $matches[ 0 ];
            }
            $seek += self::BUFFER_SIZE;
        }
        return false;
    }
    
    public function getFileName()
    {
        return $this->fileName;
    }
    
    public function getEOL()
    {
        return $this->EOL;
    }

    /**
     * Clear all file content.
     * 
     * @return bool
     */
    public function clear()
    {
        return ftruncate( $this->fp, 0 );
    }

    /**
     * Remove all file content and write string.
     *
     * @param string $string
     * @return bool
     */
    public function rewrite( $string )
    {
        return $this->clear() && $this->append( $string );
    }

    /**
     * Prepend content to start of file content.
     *
     * @param string $string
     * @return bool
     */
    public function prepend( $string )
    {
        if( $string == '' ) {
            return true;
        }
        $readAll = $this->readAll();
        $this->clear();
        return fwrite( $this->fp, $string . $readAll ) !== false;
    }

    /**
     * Append content to end of file content.
     *
     * @param string $string
     * @return bool|int
     */
    public function append( $string )
    {
        if( $string == '' ) {
            return true;
        }
        return fwrite( $this->fp, $string ) !== false;
    }

    /**
     * @param int $seek
     * @param int $length
     * @return bool|string
     */
    public function read( $seek, $length )
    {
        fseek( $this->fp, $seek );
        if( ( $fread = fread( $this->fp, $length ) ) === false ) {
            return false;
        }
        return $fread;
    }

    /**
     * Reading all and returns as string.
     * 
     * @return bool|string
     */
    public function readAll()
    {
        $readAll = '';
        fseek( $this->fp, 0 );
        while( ! feof( $this->fp ) ) {
            if( ( $fread = fread( $this->fp, self::BUFFER_SIZE ) ) === false ) {
                return false;
            }
            $readAll .= $fread;
        }
        return ( $readAll == '' ) ? false : $readAll;
    }

    /**
     * Reading all lines and returns as array.
     * 
     * @return array|bool
     */
    public function readAllAsLines()
    {
        $lines = array();
        fseek( $this->fp, 0 );
        while( ! feof( $this->fp ) ) {
            $lines[] = trim( stream_get_line( $this->fp, self::LINE_MAX_SIZE, $this->EOL ) );
        }
        return ( empty( $lines ) ) ? false : $lines;
    }

    /**
     * Reading line by number.
     * 
     * @param int $lineNum
     * @return bool|string
     */
    public function readLine( $lineNum )
    {
        fseek( $this->fp, 0 );
        for( $linesPassed = 0; $linesPassed < $lineNum; ++$linesPassed ) {
            if( ! feof( $this->fp ) ) {
                stream_get_line( $this->fp, self::LINE_MAX_SIZE, $this->EOL );
            }
            else {
                return false;
            }
        }
        return trim( stream_get_line( $this->fp, self::LINE_MAX_SIZE, $this->EOL ) );
    }

    /**
     * Read specified line and then remove it from the file.
     * 
     * @param $lineNum
     * @return bool|string
     */
    public function readAndRemoveLine( $lineNum )
    {
        $resultLine = false;
        
        $tmpCopyFileName = tempnam( sys_get_temp_dir(), 'copy' );
        $this->copyTo( $tmpCopyFileName );
        $this->clear();
        
        $fpTmpCopy = fopen( $tmpCopyFileName, 'a+' );
        for( $linesPassed = 0; $linesPassed < $linesPassed + 1; ++$linesPassed ) {
            if( feof( $fpTmpCopy ) ) {
                break;
            }
            $line = stream_get_line( $fpTmpCopy, self::LINE_MAX_SIZE, $this->EOL );
            if( $linesPassed == $lineNum ) {
                $resultLine = $line;
            }
            else {
                if( ! empty( $line ) ) {
                    fwrite( $this->fp, "{$line}{$this->EOL}" );
                }
            }
        }
        fclose( $fpTmpCopy );
        return $resultLine;
    }

    /**
     * Remove line from the file.
     *
     * @param int $lineNum
     * @return bool
     */
    public function removeLine( $lineNum )
    {
        return $this->readAndRemoveLine( $lineNum ) !== false;
    }

    /**
     * Replace file content. (Full equivalent of native php function preg_replace.)
     *
     * @param string $regex
     * @param string $replacement
     * @param int $limit
     * @return bool
     */
    public function preg_replace( $regex, $replacement, $limit = -1 )
    {
        $replaced = preg_replace( $regex, $replacement, $this->readAll(), $limit );
        return $this->rewrite( $replaced );
    }

    /**
     * Safely copy contents from current file to destination file.
     *
     * @param string $destFileName
     * @return bool
     */
    public function copyTo( $destFileName )
    {
        try {
            $destFile = new File( $destFileName, $this->isLocked );
            return $destFile->rewrite( $this->readAll() ) && $destFile->close();
        }
        catch( FileOpeningFailedException $e ) {
            return false;
        }
    }

    /**
     * Closing fopen resource and lock (if locked).
     * 
     * @return bool
     */
    public function close()
    {
        if( $this->isLocked ) {
            flock( $this->fp, LOCK_UN );
        }
        if( is_resource( $this->fp ) ) {
            return fclose( $this->fp );
        }
        return true;
    }
}