<?php
namespace Brocoder\FileSystem;

class SFileLocked
{
    /**
     * @param string $fileName
     * @return bool
     * @see SFile::clear()
     */
    public static function clear( $fileName )
    {
        try {
            return SFile::clear( $fileName, true );
        }
        catch( FileOpeningFailedException $e ) {
            return false;
        }
    }

    /**
     * @param string $fileName
     * @param string $content
     * @return bool
     * @see SFile::rewrite()
     */
    public static function rewrite( $fileName, $content )
    {
        try {
            return SFile::rewrite( $fileName, $content, true );
        }
        catch( FileOpeningFailedException $e ) {
            return false;
        }
    }

    /**
     * @param string $fileName
     * @param string $content
     * @return bool
     * @see SFile::prepend()
     */
    public static function prepend( $fileName, $content )
    {
        try {
            return SFile::prepend( $fileName, $content, true );
        }
        catch( FileOpeningFailedException $e ) {
            return false;
        }
    }

    /**
     * @param string $fileName
     * @param string $content
     * @return bool
     * @see SFile::append()
     */
    public static function append( $fileName, $content )
    {
        try {
            return SFile::append( $fileName, $content, true );
        }
        catch( FileOpeningFailedException $e ) {
            return false;
        }
    }

    /**
     * @param string $fileName
     * @param int $seek
     * @param int $length
     * @return bool|string
     * @see SFile::read()
     */
    public static function read( $fileName, $seek, $length )
    {
        try {
            return SFile::read( $fileName, $seek, $length, true );
        }
        catch( FileOpeningFailedException $e ) {
            return false;
        }
    }

    /**
     * @param string $fileName
     * @return bool|string
     * @see SFile::readAll()
     */
    public static function readAll( $fileName )
    {
        try {
            return SFile::readAll( $fileName, true );
        }
        catch( FileOpeningFailedException $e ) {
            return false;
        }
    }

    /**
     * @param string $fileName
     * @param int $lineNum
     * @return bool|string
     * @see SFile::readLine()
     */
    public static function readLine( $fileName, $lineNum )
    {
        try {
            return SFile::readLine( $fileName, $lineNum, true );
        }
        catch( FileOpeningFailedException $e ) {
            return false;
        }
    }

    /**
     * @param string $fileName
     * @return bool|array
     * @see SFile::readAllAsLines()
     */
    public static function readAllAsLines( $fileName )
    {
        try {
            return SFile::readAllAsLines( $fileName, true );
        }
        catch( FileOpeningFailedException $e ) {
            return false;
        }
    }

    /**
     * @param string $fileName
     * @param $lineNum
     * @return string|bool
     * @see SFile::readAndRemoveLine()
     */
    public static function readAndRemoveLine( $fileName, $lineNum )
    {
        try {
            return SFile::readAndRemoveLine( $fileName, $lineNum, true );
        }
        catch( FileOpeningFailedException $e ) {
            return false;
        }
    }
    
    /**
     * @param string $fileName
     * @param $lineNum
     * @return bool
     * @see SFile::removeLine()
     */
    public static function removeLine( $fileName, $lineNum )
    {
        try {
            return SFile::removeLine( $fileName, $lineNum, true );
        }
        catch( FileOpeningFailedException $e ) {
            return false;
        }
    }

    /**
     * @param string $sourceFileName
     * @param string $destFileName
     * @return bool
     * @see SFile::copyTo()
     */
    public static function copyTo( $sourceFileName, $destFileName )
    {
        try {
            return SFile::copyTo( $sourceFileName, $destFileName, true );
        }
        catch( FileOpeningFailedException $e ) {
            return false;
        }
    }

    /**
     * @param string $fileName
     * @param string $pattern
     * @param string $replacement
     * @param int $limit
     * @return bool
     * @see SFile::preg_replace()
     */
    public static function preg_replace( $fileName, $pattern, $replacement, $limit = -1 )
    {
        try {
            return SFile::preg_replace( $fileName, $pattern, $replacement, $limit, true );
        }
        catch( FileOpeningFailedException $e ) {
            return false;
        }
    }
}