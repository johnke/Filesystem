<?php
namespace Brocoder\FileSystem;

interface FileI
{
    /**
     * @param string $fileName
     */
    public function __construct( $fileName );

    /**
     * @return bool
     */
    public function clear();

    /**
     * @param string $string
     * @return bool
     */
    public function rewrite( $string );

    /**
     * @param string $string
     * @return bool
     */
    public function prepend( $string );

    /**
     * @param string $string
     * @return bool|int
     */
    public function append( $string );

    /**
     * @param int $seek
     * @param int $length
     * @return bool|string
     */
    public function read( $seek, $length );

    /**
     * @return bool|string
     */
    public function readAll();

    /**
     * @return array|bool
     */
    public function readAllAsLines();

    /**
     * @param int $lineNum
     * @return bool|string
     */
    public function readLine( $lineNum );

    /**
     * @param $lineNum
     * @return string
     */
    public function readAndRemoveLine( $lineNum );

    /**
     * @param int $lineNum
     */
    public function removeLine( $lineNum );

    /**
     * @param string $regex
     * @param string $replacement
     * @param int $limit
     * @return bool
     */
    public function preg_replace( $regex, $replacement, $limit = -1 );

    /**
     * @param string $destFileName
     * @return bool
     */
    public function copyTo( $destFileName );

    /**
     * @return bool
     */
    public function close();
}