<?php
namespace Brocoder\FileSystem\Tests;

use Brocoder\FileSystem\File;
use Brocoder\FileSystem\SFileLocked;
use PHPUnit\Framework\TestCase;
use ReflectionObject;

require_once __DIR__ . '/../vendor/autoload.php';

class FileTest extends TestCase implements FileTestI
{
    private static $testFileName;
    private const TEST_CONTENT = "0\r\n1\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n8\r\n9\r\n";
    
    protected function setUp()
    {
        self::$testFileName = sys_get_temp_dir() . '/test_file.txt';
        file_put_contents( self::$testFileName, self::TEST_CONTENT );
    }
    
    public function tearDown()
    {
        unlink( self::$testFileName );
    }
    
    public function testLock()
    {
        $file = new File( self::$testFileName, true );
        $concurrentFp = fopen( self::$testFileName, 'a+' );
        $this->assertFalse( flock( $concurrentFp, LOCK_EX | LOCK_NB ), 'We abnormally got a lock' );
        $file->close();
        $this->assertTrue( flock( $concurrentFp, LOCK_EX | LOCK_NB ), 'Can\'t get a lock' );
        fclose( $concurrentFp );
    }

    public function testClear()
    {
        $this->assertTrue( SFileLocked::clear( self::$testFileName ) );
        $this->assertEmpty( file_get_contents( self::$testFileName ), 'Something wrong with clear() function' );
    }
    
    public function testRewrite()
    {
        $assertedString = 'rewrote';
        $this->assertTrue( SFileLocked::rewrite( self::$testFileName, $assertedString ) );
        $this->assertEquals(
            $assertedString,
            file_get_contents( self::$testFileName ),
            'Something wrong with rewrite() function'
        );
    }

    public function testPrepend()
    {
        $assertedString = 'prepended';
        $this->assertTrue( SFileLocked::prepend( self::$testFileName, $assertedString ) );
        $this->assertTrue(
            ( boolean ) preg_match( "/^{$assertedString}/", file_get_contents( self::$testFileName ) ),
            'Something wrong with prepend() function'
        );
    }

    public function testAppend()
    {
        $assertedString = 'appended';
        $this->assertTrue( SFileLocked::append( self::$testFileName, $assertedString ) );
        $this->assertTrue(
            ( boolean ) preg_match( "/{$assertedString}$/", file_get_contents( self::$testFileName ) ),
            'Something wrong with prepend() function'
        );
    }
    
    public function testRead()
    {
        $testContentLines = $this->getTestContentAsLines();
        $testContentFirstLine = $testContentLines[ 0 ];
        $readTestContentFirstLine = SFileLocked::read( self::$testFileName, 0, strlen( $testContentFirstLine ) );
        $this->assertNotEquals( false, $readTestContentFirstLine );
        $this->assertEquals( $testContentFirstLine, $readTestContentFirstLine, 'Something wrong with read() function' );
    }
    
    public function testReadAll()
    {
        $readAll = SFileLocked::readAll( self::$testFileName );
        $this->assertNotEquals( false, $readAll );
        $this->assertEquals(
            self::TEST_CONTENT,
            $readAll,
            'Something wrong with readAll() function'
        );
    }
    
    public function testReadAllAsLines()
    {
        $readAllAsLines = SFileLocked::readAllAsLines( self::$testFileName );
        $this->assertNotEquals( false, $readAllAsLines );
        foreach( $this->getTestContentAsLines() as $index => $line ) {
            $this->assertEquals(
                trim( $line ),
                $readAllAsLines[ $index ],
                'Something wrong with readAllAsLines() function'
            );
        }
    }
    
    public function testReadLine()
    {
        $assertionLineIndex = 1;
        $readLine = SFileLocked::readLine( self::$testFileName, $assertionLineIndex );
        $this->assertNotEquals( false, $readLine );
        $this->assertEquals(
            $this->getTestContentAsLines()[ $assertionLineIndex ],
            $readLine,
            'Something wrong with readLine() function'
        );
    }

    public function testReadAndRemoveLine()
    {
        $testedLineNum = mt_rand( 0, count( $this->getTestContentAsLines() ) - 1 );
        $removedString = SFileLocked::readAndRemoveLine( self::$testFileName, $testedLineNum );
        $this->assertEquals( $testedLineNum, $removedString );
        $this->assertFalse( in_array( $testedLineNum, SFileLocked::readAllAsLines( self::$testFileName ), true ) );
    }

    public function testPregReplace()
    {
        $this->assertTrue( SFileLocked::preg_replace( self::$testFileName, '/.*?\n/', '' ) );
        $this->assertFalse(
            SFileLocked::readAll( self::$testFileName ),
            'Something wrong with preg_replace() function'
        );
    }
    
    public function testCopyTo()
    {
        $sourceFileName = self::$testFileName;
        $destFileName = sys_get_temp_dir() . '/test_file_copied.txt';
        $this->assertTrue( SFileLocked::copyTo( $sourceFileName, $destFileName ) );
        $this->assertEquals( self::TEST_CONTENT, file_get_contents( $destFileName ) );
        $this->assertEquals( self::TEST_CONTENT, file_get_contents( $sourceFileName ) );
        unlink( $destFileName );
    }

    public function testRemoveLine()
    {
        $testedLineNum = mt_rand( 0, count( $this->getTestContentAsLines() ) - 1 );
        SFileLocked::removeLine( self::$testFileName, $testedLineNum );
        $this->assertFalse( in_array( $testedLineNum, SFileLocked::readAllAsLines( self::$testFileName ), true ) );
    }

    public function testClose()
    {
        $file = new File( self::$testFileName, true );
        $this->assertEquals(
            'stream',
            get_resource_type( $this->getPropertyValueFromObject( $file, 'fp' ) ),
            'Before closing, fopen resource must be "stream"'
        );
        $file->close();
        $this->assertEquals(
            'Unknown',
            get_resource_type( $this->getPropertyValueFromObject( $file, 'fp' ) ),
            'After closing, fopen resource must be "Unknown"'
        );

        $file = new File( self::$testFileName, true );
        $concurrentFp = fopen( self::$testFileName, 'a+' );
        $this->assertFalse( flock( $concurrentFp, LOCK_EX | LOCK_NB ), 'We got a lock, although it abnormally' );
        $file->close();
        $this->assertTrue( flock( $concurrentFp, LOCK_EX | LOCK_NB ), 'Can\'t get a lock' );
        fclose( $concurrentFp );
    }
    
    private function getPropertyValueFromObject( Object $object, $propertyName )
    {
        $ref = new ReflectionObject( $object );
        $prop = $ref->getProperty( $propertyName );
        $prop->setAccessible( true );
        return $prop->getValue( $object );
    }

    /**
     * @return array
     */
    private function getTestContentAsLines()
    {
        return explode( "\r\n", trim( self::TEST_CONTENT ) );
    }
}